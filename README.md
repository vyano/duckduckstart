DuckDuckStart
============

https://duckduckstart.com/

Use [DuckDuckGo](https://duckduckgo.com/) for !bangs and [StartPage](https://www.startpage.com/) for everything else.

This is now hosted on GitLab, https://gitlab.com/vyano/duckduckstart
